/**
 * 
 */
package com.abocode.jfaster.web.system.domain.repository;

import com.abocode.jfaster.core.domain.repository.CommonRepository;


/**
 * 日志Service接口
 * @author  方文荣
 *
 */
public interface LogService extends CommonRepository {

}
