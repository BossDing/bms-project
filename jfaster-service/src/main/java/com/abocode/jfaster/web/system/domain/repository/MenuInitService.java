package com.abocode.jfaster.web.system.domain.repository;

import com.abocode.jfaster.core.domain.repository.CommonRepository;

/**
 * 
 * @author  张代浩
 *
 */
public interface MenuInitService extends CommonRepository {
	
	public void initMenu();
}
