package com.abocode.jfaster.web.system.domain.repository.persistence.hibernate;

import com.abocode.jfaster.core.domain.repository.persistence.hibernate.CommonRepositoryImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("jobService")
@Transactional
public class JobServiceImpl extends CommonRepositoryImpl {


}